package nl.utwente.di.temperature;

public class Converter {
    public static double toFahrenheit(double celsius) {
        return celsius * 1.8 + 32;
    }
}
